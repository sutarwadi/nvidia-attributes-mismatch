#include <stdio.h>

__global__ void test()
{
  int device;
  cudaError_t error_id = cudaGetDevice(&device);
  int val0;
  cudaDeviceGetAttribute(&val0, cudaDevAttrPciBusId, device);
  printf("   INFO  PCI BUS ID    =           %d \n", val0);
}

int main()
{
  int deviceCount = 0;
  cudaError_t error_id = cudaGetDeviceCount(&deviceCount);
  if (error_id != cudaSuccess) {
    return EXIT_FAILURE;
  }
  cudaSetDevice(0);
  int val0;
  cudaDeviceGetAttribute(&val0, cudaDevAttrPciBusId, 0);
  printf("   INFO  PCI BUS ID    =           %d \n", val0);

  test<<<1,1>>>();
  cudaDeviceSynchronize();
}

